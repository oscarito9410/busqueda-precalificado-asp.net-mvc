﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Formularios.Models
{
    public class Busqueda
    {
        public int id { get; set; }
        public string query { get; set; }
        public int tipo { get; set; }  
    }
}