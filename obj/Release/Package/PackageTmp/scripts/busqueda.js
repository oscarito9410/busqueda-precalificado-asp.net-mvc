﻿$("#query").autocomplete({
    source: function (request, response) {
        $("#tipo").change(function () {
            $("#query").val("");

        });

        $.post("/Home/busqueda/",
        {
            query: $("#query").val(),tipo:$("#tipo").val()
        },
        function (data) {
            console.log(data);
            response($.map(data, function (item) {
                
                var label=parseInt($("#tipo").val())==1?  item.folio:item.nombre;
                return {
                    label: label,
                    value: item.id_cliente
                }
            }));
        });
    },
    select: function (event, ui) {
        event.preventDefault();
        $("#query").val(ui.item.label);
        $("#id").val(ui.item.value);
        $("#formBusqueda").submit();

    },
    focus: function (event, ui) {
        event.preventDefault();
        $("#query").val(ui.item.label);
        $("#id").val(ui.item.value);
    },
    minLength: 2
});