﻿using Formularios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Formularios.Controllers
{
    public class HomeController : Controller
    {
        private Formularios.Models.sistem80_precalificacionEntities1 db = new Formularios.Models.sistem80_precalificacionEntities1();

        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "Busqueda precalificado";
            return View();
        }
        [HttpPost]
        public ActionResult busqueda(String query,int tipo)
        {
            if (string.IsNullOrEmpty(query))
            {
                return Json(new { success = false, responseText = "Ingresa una busqueda" });
            }

            var result = db.cliente.Where(x => tipo==1?x.folio.StartsWith(query): x.nombre.StartsWith(query)).ToList();

            return Json(result);

        }
        [HttpPost]
        public ActionResult Index(Busqueda busqueda)
        {
            var precandidato = db.cliente.Find(busqueda.id);

            if (precandidato == null)
            {
                return HttpNotFound();
            }
            else
            {
             
                ViewBag.candidato = precandidato;
            }
            //  return View(precandidato);
            return View(busqueda);
        }
    }
}